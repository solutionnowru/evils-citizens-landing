import * as functions from 'firebase-functions';
import * as cors from 'cors';
import { firestore, initializeApp } from 'firebase-admin';

initializeApp();
const withCors = (callback: (req: functions.https.Request, res: functions.Response<any>) => void) => {
  return (req: functions.https.Request, res: functions.Response<any>) => {
    cors({ origin: true })(req, res, () => {
      callback(req, res)
    });
  }
}

export const signin = functions.https.onRequest(withCors(async (request, response) => {
  const email = request.body.data;
  try {
    await firestore().collection('users').doc(email).set({ date: new Date()});
  } catch (err) {
    console.log(err)
    response.send({ data: 'failed' });
    return
  }
  response.send({data: 'ok'});
}));
